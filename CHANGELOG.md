v2.0.0
包管理工具由npm切换为ohpm 
适配DevEco Studio: 3.1 Beta2 (3.1.0.400)
适配SDK: API9 Release(3.2.11.9)

## v1.0.2

- 适配DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）。

## v1.0.1

  1. 升级API9
  
  2. 修改FA模式为Stage模式

## v1.0.0

- 已实现功能
    1. 压缩
    2. 解压