# brotli

## Introduction
Brotli is a general-purpose lossless compression algorithm.

## Display Effects
<img src="preview/preview.gif"/>

## How to Install
```shell
ohpm install brotli-js 
ohpm install @types/brotli --save-dev // Install @types/brotli to prevent import syntax errors due to missing type declarations in the brotli package.
```
For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use
> **NOTE**
>
> This library does not support compression or decompression of Chinese characters.

 ```
   import brotli from 'brotli-js';
 ```

```
@Entry
@Component
struct Index {
  @State mgs_compressed:string = "Null"
  @State mgs_decompressed:string = "Null"

private stringToBytes(str): Int8Array  {
  let out = new Int8Array(str.length);
  for (let i = 0; i < str.length; ++i) out[i] = str.charCodeAt(i);
  return out;
}

 private bytesToString(bytes): string {
   return String.fromCharCode.apply(null, new Uint16Array(bytes));
 }

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
      Text('Brotli').fontSize(15).fontColor(Color.Black)
      Text('Brotli is a generic-purpose lossless compression algorithm that compresses data using a combination of a modern variant of the LZ77 algorithm, Huffman coding and 2nd order context modeling, with a compression ratio comparable to the best currently available general-purpose compression methods. It is similar in speed with deflate but offers more dense compression.')
        .textOverflow({ overflow: TextOverflow.None })
        .fontSize(12).border({ width: 1 }).padding(10)
      Text('Compress')
        .fontSize(20)
        .fontWeight(FontWeight.Bold)
        .onClick(() => {
          const str = 'test txt'
          const buf = new ArrayBuffer(str.length)
          const bufView = new Uint8Array(buf)

          for (let i = 0, strLen = str.length; i < strLen; i++) {
            bufView[i] = str.charCodeAt(i)
          }

          const compressed = brotli.compressArray(bufView, 6)
          const decompressed = brotli.decompressArray(compressed)
          const restoredStr = String.fromCharCode.apply(null, decompressed)
          this.mgs_compressed = compressed
          this.mgs_decompressed = restoredStr
        })
      Text('Encoding result: '+this.mgs_compressed)
        .textOverflow({ overflow: TextOverflow.None })
        .fontSize(12).border({ width: 1 }).padding(10)
      Text('Decoding result: '+this.mgs_decompressed)
        .textOverflow({ overflow: TextOverflow.None })
        .fontSize(12).border({ width: 1 }).padding(10)
    }
    .width('100%')
    .height('100%')
  }
}

```
## Available APIs

1. Compression API
```
  export function compressArray(uint: Uint8Array, level?: number)
```
2. Decompression API
```
  export function decompressArray(compressed)
```

## Constraints
This project has been verified in the following versions:

- Deveco Studio:4.0 (4.0.3.512),SDK:API10 (4.0.10.9)
- DevEco Studio: 3.1 Beta2(3.1.0.400), SDK: API9 Release(3.2.11.9)
- DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)
## Directory Structure
```
│  config.json # Application configuration file
├─entry 
│	└─src
│		└─main
│        	├─ets
│          		└─MainAbility
│              		└─app.ets # Application entry 
│              		└─pages
│                      └─index.ets # Example
│              
│           ├─resources
│               └─base
│                   ├─element
│                   │    └─string.json # Default string file
│                   └─media
│                        └─icon.png # Default application icon
```

## How to Contribute
If you find any problem during the use, submit an [Issue](https://gitee.com/openharmony-sig/brotli/issues) or a [PR](https://gitee.com/openharmony-sig/brotli/pulls) to us.

## License
This project is licensed under [Apache License 2.0](https://gitee.com/openharmony-sig/brotli/blob/master/LICENSE).

  
