# brotli

## 简介
Brotli 是一种通用无损压缩算法

## 效果展示
<img src="preview/preview_zh.gif"/>

## 下载安装
```shell
ohpm install brotli-js 
ohpm install @types/brotli --save-dev //import brotli 的时候语法报错。其原因是brotli包内不含类型声明，需要 @types/brotli 下载这个包的声明文件，从而解决语法的报错。
```
OpenHarmony ohpm 环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 使用说明
**注意本库暂不支持中文字符的压缩/解压缩**
 ```
   import brotli from 'brotli-js';
 ```

```
@Entry
@Component
struct Index {
  @State mgs_compressed:string = "空"
  @State mgs_decompressed:string = "空"

private stringToBytes(str): Int8Array  {
  let out = new Int8Array(str.length);
  for (let i = 0; i < str.length; ++i) out[i] = str.charCodeAt(i);
  return out;
}

 private bytesToString(bytes): string {
   return String.fromCharCode.apply(null, new Uint16Array(bytes));
 }

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
      Text('Brotli').fontSize(15).fontColor(Color.Black)
      Text('Brotli is a generic-purpose lossless compression algorithm that compresses data using a combination of a modern variant of the LZ77 algorithm, Huffman coding and 2nd order context modeling, with a compression ratio comparable to the best currently available general-purpose compression methods. It is similar in speed with deflate but offers more dense compression..')
        .textOverflow({ overflow: TextOverflow.None })
        .fontSize(12).border({ width: 1 }).padding(10)
      Text('压缩')
        .fontSize(20)
        .fontWeight(FontWeight.Bold)
        .onClick(() => {
          const str = 'test txt'
          const buf = new ArrayBuffer(str.length)
          const bufView = new Uint8Array(buf)

          for (let i = 0, strLen = str.length; i < strLen; i++) {
            bufView[i] = str.charCodeAt(i)
          }

          const compressed = brotli.compressArray(bufView, 6)
          const decompressed = brotli.decompressArray(compressed)
          const restoredStr = String.fromCharCode.apply(null, decompressed)
          this.mgs_compressed = compressed
          this.mgs_decompressed = restoredStr
        })
      Text('编码结果：'+this.mgs_compressed)
        .textOverflow({ overflow: TextOverflow.None })
        .fontSize(12).border({ width: 1 }).padding(10)
      Text('解码结果：'+this.mgs_decompressed)
        .textOverflow({ overflow: TextOverflow.None })
        .fontSize(12).border({ width: 1 }).padding(10)
    }
    .width('100%')
    .height('100%')
  }
}

```
## 接口说明

1. 压缩接口
```
  export function compressArray(uint: Uint8Array, level?: number)
```
2. 解压接口
```
  export function decompressArray(compressed)
```

## 约束与限制
在下述版本验证通过：

- Deveco Studio:4.0 (4.0.3.512),SDK:API10 (4.0.10.9)
- DevEco Studio: 3.1 Beta2(3.1.0.400), SDK: API9 Release(3.2.11.9)
- DevEco Studio: 4.1 Release (4.1.0.400), SDK: API 11 Release (4.1.7.5)
- DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)

## 目录结构
```
│  config.json #应用程序配置文件
├─entry 
│	└─src
│		└─main
│        	├─ets
│          		└─MainAbility
│              		└─app.ets #应用程序入口  
│              		└─pages
│                      └─index.ets #调用实例
│              
│           ├─resources
│               └─base
│                   ├─element
│                   │    └─string.json #默认string文件
│                   └─media
│                        └─icon.png #默认程序图标
```

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/ohos_ftp_srv/issues) 给组件，当然，也非常欢迎发 [PR](https://gitee.com/openharmony-sig/ohos_ftp_srv/pulls)共建 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/brotli/blob/master/LICENSE) ，请自由地享受和参与开源。

  
